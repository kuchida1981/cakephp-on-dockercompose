# CakePHP on Docker Compose

Docker ComposeでCakePHP開発するためのファイル群です.


## Description

CakePHPのデバッグ環境 (PHP実行環境) とMySQLをまとめてDockerでセットアップできます.


## Requirement

* Docker
* Docker Compose

必須ではありませんが, あとDocker Machine.

動作確認や Usage での説明は, Docker Machine での使用を想定しています.


## Usage

使い方を簡単に示します.


### リポジトリを取得する

このリポジトリを取得してください.

```bash
$ git clone https://kuchida1981@bitbucket.org/kuchida1981/cakephp-on-dockercompose.git
```

クローンしたリポジトリをカレントディレクトリにします.

```bash
$ cd cakephp-on-dockercompose
```


### 環境変数を設定する

`.env` ファイルを開いて, 変数 `WORK_DIRPATH` を設定してください.
通常は現在のディレクトリのフルパスを指定します.
たとえば, `/home/kosuke/tmp/cakephp-on-dockercompose` とします.

DockerホストがDocker Machineによるものなら, Dockerホストから見たパスとしてください.
Docker Machineの実行環境がWindowsであれば `/home`を `/c/Users` に,
Linuxであれば `/hosthome` に置き換える必要があるはずです.

そのほかの環境変数については, 必要に応じて修正してください.

`CAKEAPP_DIR`
はComposerによるCakePHPプロジェクトの作成先のディレクトリ名を指定します.


### CakePHPプロジェクトを作成する

続けて次のコマンドを実行します.

```bash
$ docker-compose run --rm web \
    composer create-project --prefer-dist -n cakephp/app CAKEAPP_DIRで指定したフォルダ名
```

初回実行時であれば, PHPデバッグサーバのためのイメージをビルドしたあとで,
Composerによるプロジェクト作成が行われます.


### デバッグ用サーバを起動する

CakePHPデバッグ用サーバを起動するために, 次のコマンドを実行します.

```bash
$ docker-compose up -d
```

DockerホストのIPアドレスを確認して, `http://docker-host-ip:8765`
へアクセスできることを確認します[^1].


### ヒント 1 データベースの設定

CakePHPのトップページのデータベース設定の箇所が次のように表示されている場合,
データベースの設定が必要です.

> CakePHP is NOT able to connect to the database.

`.env` のMySQL関連の設定を変えていない場合,
`app/config/app.php` の `DetaSources` の設定を次のように修正します.

```php
'Datasources' => [
    'default' => [
        'className' => 'Cake\Database\Connection',
        'driver' => 'Cake\Database\Driver\Mysql',
        'persistent' => false,
        'host' => 'db', // このへんを修正しています
        'username' => 'cakephp', // このへんを修正しています
        'password' => 'secret', // このへんを修正しています
        'database' => 'cakephp', // このへんを修正しています
```


### ヒント 2 cakeコマンドの実行

CakePHP開発に必須になる `cake` コマンドは, 次のように実行できます.

```bash
$ docker-compose run --rm web app/bin/cake
```

### ヒント 3 mysqlへの接続

MySQLへ接続するには `docker-compose exec db mysql -U cakephp -p` を実行します.

```
$ docker-compose exec db mysql -U cakephp -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 4
Server version: 5.7.17 MySQL Community Server (GPL)

Copyright (c) 2000, 2016, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
```


### ヒント 4 エイリアスの設定

`cake` にせよ, `mysql` にせよ, それなりに使うコマンドなので,
エイリアスを作っておいたほうが楽かもしれません.

```bash
alias cake='docker-compose run --rm web ${CAKEAPP_DIR}/bin/cake'
alias mysql='docker-compose exec db mysql'
```

[^1]: 通常は, `127.0.0.1` です. Docker Machineを使っているなら,
`docker-machine ip` で確認できます.